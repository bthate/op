# OP - Object Programming (test_object.py)
#
# this file is placed in the public domain

"test object functions"

# imports

import unittest

from op.dbs import last
from op import O, Object, load, save

# classes

class Test_Object(unittest.TestCase):

    "object tests"

    def testO(self):
        "test core object"
        o = O()
        self.assertEqual(type(o), O)

    def testObject(self):
        "test Object"
        o = Object()
        self.assertEqual(type(o), Object)

    def test_intern1(self):
        "test type"
        o = Object()
        self.assertTrue(o.__type__)

    def test_intern2(self):
        "test default"
        o = Object()
        self.assertFalse(o)

    def test_intern3(self):
        "test print"
        o = Object()
        self.assertTrue("<op.Object object at" in repr(o))

    def test_intern4(self):
        "test type in print"
        o = Object()
        self.assertTrue(o.__type__ in str(type(o)))

    def test_intern5(self):
        "assert id"
        o = Object()
        self.assertTrue(o.__id__)

    def test_empty(self):
        "test not object"
        o = Object()
        self.assertTrue(not o)

    def test_final(self):
        "test last object"
        o = Object()
        o.last = "bla"
        last(o)
        self.assertEqual(o.last, "bla")

    def test_stamp(self):
        "test type after save"
        o = Object()
        save(o)
        self.assertTrue(o.__type__)

    def test_attribute(self):
        "test if attribute is loaded"
        o = Object()
        o.bla = "test"
        p = save(o)
        oo = Object()
        load(oo, p)
        self.assertEqual(oo.bla, "test")

    def test_changeattr(self):
        "test attribute no changed"
        o = Object()
        o.bla = "test"
        p = save(o)
        oo = Object()
        load(oo, p)
        oo.bla = "mekker"
        pp = save(oo)
        ooo = Object()
        load(ooo, pp)
        self.assertEqual(ooo.bla, "mekker")

    def test_last(self):
        "test last version"
        o = Object()
        o.bla = "test"
        save(o)
        oo = Object()
        last(oo)
        self.assertEqual(oo.bla, "test")

    def test_lastest(self):
        "test lastest of latest"
        o = Object()
        o.bla = "test"
        save(o)
        oo = Object()
        last(oo)
        oo.bla = "mekker"
        save(oo)
        ooo = Object()
        last(ooo)
        self.assertEqual(ooo.bla, "mekker")
