# OP - Object Programming (setup.py)
#
# this file is placed in the public domain

"setuptools setup()"

from setuptools import setup

def readme():
    "read readme file"
    with open('README.rst') as file:
        return file.read()

setup(
    name='objectprogramming',
    version='4',
    url='https://bitbucket.org/bthate/op/',
    author='Bart Thate',
    author_email='bthate@dds.nl',
    description="Object Programming",
    long_description=readme(),
    license='Public Domain',
    packages=["op"],
    scripts=["bin/op", "bin/opctl", "bin/opd", "bin/opinst", "bin/opudp"],
    zip_safe=True,
    classifiers=['Development Status :: 3 - Alpha',
                 'License :: Public Domain',
                 'Operating System :: Unix',
                 'Programming Language :: Python',
                 'Topic :: Utilities'
                ]
)
