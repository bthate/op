README
######

Welcome to Object Programming, a python3 object programming library.

Object Programming is a pure python3 object programming library you can
use to program objects with. Object Programming uses a programming by contract
where the methods are factored out into functions that use an object as the 
first argument. This to provide a clean object, which has no methods to clutter
the namespace of the object. A "clean", still has hidden methods, loadable from
JSON file, object that provides load/save methods to other classes derived
from Object.

Object Programming provides the 'op' program as a demo of the library,
You can use it as a IRC bot running as a background daemon for 24/7 a day
presence in a IRC channel, display RSS feeds or act as a UDP to IRC gateway
You can also use it as a cli to program your own commands for it, have it log
objects on disk and search them or use it to scan emails for correspondence
analysis. The demo uses a JSON in file database with a versioned readonly
storage so it is more persistent and it reconstructs objects based on type
information in the path.

Object Programming is placed in the Public Domain, no COPYRIGHT, no LICENSE.

INSTALL
=======

installation is through pypi:

::

 > sudo pip3 install objectprogramming

OBJECT PROGRAMMING
==================

Object Programming  provides a “move methods to functions”, if you are used to
functional programming you’ll like it (or not)::

 obj.method(*args) -> method(obj, *args)

not::

 >>> from op import Object
 >>> o = Object()
 >>> o.set("key", "value")
 >>> o.key
 'value'

but::

 >>> from op import Object, set
 >>> o = Object()
 >>> set(o, "key", "value")
 >>> o.key
 'value'

The op module has the most basic object functions like get, set, update,
load, save etc.

A dict without methods in it is the reason to factor out methods from the
base object, it is inheritable without adding methods in inherited classes.
It also makes reading json from disk into a object easier because you don't
have any overloading taking place. Hidden methods are still available so it
is not a complete method less object, it is a pure object what __dict__ is
concerned (user defined methods)::

 >>> import op
 >>> o = op.Object()
 >>> o.__dict__
 {}

MODULES
=======

Object Programming provides the op package with the following modules:

::

    op                 - object programming library
    op.clk             - clock/repeater
    op.cmd             - commands
    op.dbs             - databases
    op.hdl             - handler
    op.irc             - internet relay chat
    op.prs             - parser
    op.rss             - rich site syndicate
    op.thr             - threads
    op.trm             - terminal
    op.udp             - udp to irc relay
    op.usr             - users
    op.utl             - utilities


CONTACT
=======

"contributed back"

| Bart Thate (bthate@dds.nl, thatebart@gmail.com)
| botfather on #dunkbots irc.freenode.net
