.. _manpage:

MANPAGE
#######

NAME
        op - object programming (demo)

SYNOPSIS

        OP provides a pure python3 IRC chat bot demo that can run as a
        background daemon for 24/7 a day presence in a IRC channel.
        
USAGE
        1) op <cmd> [mods=mod1,mod2] [-d] [-h] [-s] [-v]
        2) op cfg server=<server> channel=<channel> nick=<nick>
        3) op met <userhost>
        4) op rss <url>
        5) op ftc
        6) op cmd
        7) op mods=irc

OPTIONS
        -d              daemon mode
        -s              start a shell
        -v              be verbose
        -h              print this message
        -x              enable debug

